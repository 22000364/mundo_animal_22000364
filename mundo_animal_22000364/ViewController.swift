//
//  ViewController.swift
//  mundo_animal_22000364
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Animais : Decodable {
    let name: String
    let image_link: String
    let latin_name: String
}


class ViewController: UIViewController {

    @IBOutlet weak var Titulo: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var Latin_name: UILabel!
    

    //@IBOutlet weak var Latin_name: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoAnimal()
    }



    
    func getNovoAnimal(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animais.self){ [self]
            response in
            if let animal = response.value{
                self.imageView.kf.setImage(with: URL(string: animal.image_link))
                Titulo.text = animal.name
                Latin_name.text = animal.latin_name
            }
        }
    }
    

    
}

